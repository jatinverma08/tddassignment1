package tddAssignment1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;





public class YellingTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//R1 one person is yelling
	@Test
	public void testOnePerson() {
		
		Yelling b = new Yelling();
		
		String[] result = {"Peter"};
		String actualOutput  = b.scream(result);
		assertEquals("Peter is yelling", actualOutput);
	}

	//R2 nobody person is yelling
			@Test
				public void testNobody() {
					
				Yelling b = new Yelling();
				String result = b.scream(null);
				assertEquals("Nobody is yelling", result);
				}
			//forget to right no refactor for R1
			
	//R3 UPPERCASE person is yelling
			@Test
				public void testUpperCase() {
					
				Yelling b = new Yelling();
				String[] result = {"PETER"};
				String actualOutput  = b.scream(result);
				assertEquals("PETER IS YELLING", actualOutput);
				}

			//R4 UPPERCASE person is yelling
			@Test
				public void testArrayOfStrings() {
					
				Yelling b = new Yelling();
				String[] result = {"Peter","Kadeem"};
				String actualOutput  = b.scream(result);
				assertEquals("Peter and Kadeem are YELLING", actualOutput);
				}

}
